import entities.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import java.util.concurrent.TimeUnit;

public class Test_Yandex {

    @Test
    @DisplayName("1. Создать, удалить и проверить удаление папки.")
    public void CreateDeleteCheck() {
        RequestCreateRepository.createRepository("Repository");
        RequestDelete.deleteFileOrRepository("Repository");
        RequestCheckRepository.checkReopsitory("Repository");
    }

    @Test
    @DisplayName("2. Создать файл в папке, а потом все удалить.")
    public void CreateFileAndDelete() {
        RequestCreateRepository.createRepository("Repository2");
        RequestCreateFileInRepository.copyFileToRepository("Repository2", "Море.jpg");
        RequestDelete.deleteFileOrRepository("/Repository2/Море.jpg");
        RequestDelete.deleteFileOrRepository("Repository2");
    }

    @Test
    @DisplayName("3. Создать файл в папке, удалить, восстановить, а потом все удалить.")
    public void CreateDeleteRestore() throws InterruptedException {
        RequestCreateRepository.createRepository("Repository3");
        RequestCreateFileInRepository.copyFileToRepository("Repository3", "Море.jpg");
        RequestDelete.deleteFileOrRepository("/Repository3/Море.jpg");
        RequestRestoreFileFromBusket.restoreFileFromBusket("Море.jpg");
        TimeUnit.SECONDS.sleep(1);
        RequestDelete.deleteFileOrRepository("/Repository3/Море.jpg");
        RequestDelete.deleteFileOrRepository("Repository3");
    }

    @Test
    @DisplayName("4. Получить информацию о диске пользователя и корзины.")
    public void SpaceDiskAndBasket() {
        RequestCreateRepository.createRepository("test");
        RequestCreateFileInRepository.copyFileToRepository("test", "Зима.jpg");
        RequestCreateFileInRepository.copyFileToRepository("test", "Mount.jpg");
        int sizeFile1 = RequestSizeFile.sizeFile("test", "Зима.jpg");
        int sizeFile2 = RequestSizeFile.sizeFile("test", "Mount.jpg");
        int sizeBasketEmpty = RequestVolumeBasket.volumeBasket();
        RequestDelete.deleteFileOrRepository("/test/Зима.jpg");
        RequestDelete.deleteFileOrRepository("/test/Mount.jpg");
        int sizeBasketFull = RequestVolumeBasket.volumeBasket();
        assertThat(sizeBasketFull).isEqualTo(sizeFile1 + sizeFile2 + sizeBasketEmpty);
        RequestRestoreFileFromBusket.restoreFileFromBusket("Зима.jpg");
        RequestDelete.deleteFileOrRepository("/test/Зима.jpg");
        RequestDelete.deleteFileOrRepository("test");
    }

    @Test
    @DisplayName("5. Создать папку в папке, там файл, получить данные, удалить все, проверить удаление.")
    public void CheckDelete() throws InterruptedException {
        RequestCreateRepository.createRepository("test");
        RequestCreateRepositoryInRepository.createRepositoryInRepository("test", "foo");
        RequestCreateFileInRepository.copyFileToRepository("test/foo", "Зима.jpg");
        String typeDir = RequestTypeReposytoryOrFile.typeFile("test");
        assertThat(typeDir).isEqualTo("dir");
        RequestAsinhDelete.deleteFileOrRepository("test");
        TimeUnit.SECONDS.sleep(2);
        RequestCheckRepository.checkReopsitory("test");
        RequestCheckRepository.checkReopsitory("/test/foo");
        RequestCheckRepository.checkReopsitory("/test/foo/Зима.jpg");
    }

    @Test
    @DisplayName("6. Создать папку в папке, в ней файл. Все удалить и очистить корзину. Проверить корзину.")
    public void CheckBasket() throws InterruptedException {
        RequestCreateRepository.createRepository("test");
        RequestCreateRepositoryInRepository.createRepositoryInRepository("test", "foo");
        RequestCreateFileInRepository.copyFileToRepository("test/foo", "Зима.jpg");
        RequestAsinhDelete.deleteFileOrRepository("test");
        TimeUnit.SECONDS.sleep(2);
        RequestCleanBasket.cleanBasket();
        TimeUnit.SECONDS.sleep(10);
        int sizeBasket = RequestVolumeBasket.volumeBasket();
        assertThat(sizeBasket).isEqualTo(0);
    }
}