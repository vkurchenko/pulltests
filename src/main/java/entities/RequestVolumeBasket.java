package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestVolumeBasket {
    public static int volumeBasket() {
        Integer sizeBasket = RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/")
                .queryParam("fields", "trash_size")
                .header("Authorization", "OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .get()
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getInt("trash_size");
//        System.out.println(sizeBasket);
        return sizeBasket;
    }
}
