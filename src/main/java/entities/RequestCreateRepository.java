package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestCreateRepository {
    public static void createRepository(String repo) {
        RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/resources")
                .param("path",repo)
                .header("Authorization","OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .when()
                .put()
                .then()
                .statusCode(201);
    }

}