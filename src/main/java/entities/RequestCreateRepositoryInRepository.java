package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestCreateRepositoryInRepository {
    public static void createRepositoryInRepository(String repo, String repo2) {
        RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/resources")
                .param("path","/" + repo + "/" + repo2)
                .header("Authorization","OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .when()
                .put()
                .then()
                .statusCode(201);
    }
}
