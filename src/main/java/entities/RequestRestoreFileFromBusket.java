package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestRestoreFileFromBusket {
    public static void restoreFileFromBusket(String nameFile) {
        RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/trash/resources/restore")
                .queryParam("path", nameFile)
                .header("Authorization","OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .put()
                .then()
                .statusCode(201);
    }
}
