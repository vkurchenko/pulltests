package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestDelete {
    public static void deleteFileOrRepository(String repositoryOrFilePath) {
        RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/resources")
                .param("path",repositoryOrFilePath)
                .header("Authorization","OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .when()
                .delete()
                .then()
                .statusCode(204);
    }
}