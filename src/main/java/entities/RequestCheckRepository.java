package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestCheckRepository {
    public static void checkReopsitory(String repo) {
        RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/resources")
                .param("path",repo)
                .header("Authorization","OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .statusCode(404);
    }
}