package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestCreateFileInRepository {
    public static void copyFileToRepository(String repo, String nameFile) {
        RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/resources/copy")
                .queryParam("from", nameFile)
                .queryParam("path", "/" + repo + "/" + nameFile)
                .header("Authorization","OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(201);
    }
}