package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestTypeReposytoryOrFile {
    public static String typeFile(String repo) {
        String typeFile = RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/resources")
                .queryParam("path", repo)
                .queryParam("fields", "type")
                .header("Authorization", "OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .get()
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getString("type");
        System.out.println(typeFile);
        return typeFile;
    }
}
