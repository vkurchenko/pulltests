package entities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RequestSizeFile {
    public static int sizeFile(String repo, String nameFile) {
        Integer sizeFile = RestAssured.given()
                .log().all()
                .baseUri("https://cloud-api.yandex.net:443")
                .basePath("/v1/disk/resources")
                .queryParam("path", "/" + repo + "/" + nameFile)
                .queryParam("fields", "size")
                .header("Authorization", "OAuth AQAAAAAzPuCGAADLWywjbAZlS0BLgdiuGx5RDF0")
                .contentType(ContentType.JSON)
                .get()
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getInt("size");
//        System.out.println(sizeFile);
        return sizeFile;
    }
}
